# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :companies, only: %i[index create] do
        resources :employees, only: %i[index update] do
        end
        resources :weeks, only: %i[index create update] do
        end
      end
    end
  end
end
