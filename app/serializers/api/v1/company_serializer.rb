# frozen_string_literal: true

module Api::V1
  class CompanySerializer < Blueprinter::Base
    fields :id, :name, :business_time, :total_hours
  end
end
