# frozen_string_literal: true

module Api::V1
  # Serializador de Semana, se usa para construir el tittle y sub_title
  class WeekSerializer < Blueprinter::Base
    fields :id, :company_id, :range, :employee_availability, :monitoring_assignment

    field :title do |week|
      range_splited = week[:range].split('-')
      start_date = Date.strptime(range_splited[0], '%Y%m%d')
      end_date = Date.strptime(range_splited[1], '%Y%m%d')
      "del #{start_date.strftime('%d/%m/%Y')} al #{end_date.strftime('%d/%m/%Y')}"
    end

    field :sub_title do |week|
      range_splited = week[:range].split('-')
      start_date = Date.strptime(range_splited[0], '%Y%m%d')
      "Semana #{start_date.strftime('%W').to_i + 1} del #{start_date.strftime('%Y')}"
    end
  end
end
