# frozen_string_literal: true

module Api
  module V1
    # Controlado encargado de Listar, Crear Companias
    class CompaniesController < ApplicationController
      before_action :set_company, only: [:business_time]
      include ActionController::MimeResponds

      # Metodo encargado de Listar las companias registradas
      # GET /api/v1/companies.json
      def index
        render json: Api::V1::CompanySerializer.render(Company.all)
      end

      # Metodo encargado de Crear una Compania
      # POST /api/v1/companies.json
      def create
        company = Company.new(companies_params)
        respond_to do |format|
          if company.save
            format.json { render status: :created, json: company }
          else
            format.json do
              render status: :unprocessable_entity, json: errors_for(company)
            end
          end
        end
      end

      private
      # Se setea la compania a editar
      def set_company
        @company = Company.find(params[:id] || params[:company_id])
      end

      def companies_params
        params.require(:company).permit(
          :name
        )
      end
    end
  end
end
