# frozen_string_literal: true

module Api
  module V1
    # Controlador encargado de Listar, Crear, actualizar Semanas
    class WeeksController < ApplicationController
      before_action :set_week, only: [:update]
      include ActionController::MimeResponds

      # #Metodo encargado de listar las semanas de una compania
      # GET /api/v1/companies/1/weeks.json
      def index
        weeks = Week.get_ranges(params[:company_id])
        render json: Api::V1::WeekSerializer.render(weeks.values)
      end

      # Metodo encargado de Crear una semana para un acompania con las asignaciones y balanceo
      # POST /api/v1/companies/1/weeks.json
      def create
        new_week = Week.new(week_params)
        new_week.balance_assignments
        respond_to do |format|
          if new_week.save
            format.json { render status: :created, json: new_week }
          else
            format.json do
              render status: :unprocessable_entity,json: errors_for(new_week)
            end
          end
        end
      end

      # Metodo encargado de guardar y balancear la asignaciones de los empleados
      # PUT /api/v1/companies/1/weeks.json
      def update
        @week.employee_availability = week_params[:employee_availability]
        @week.balance_assignments
        respond_to do |format|
          if @week.save
            format.json { render status: :ok, json: @week }
          else
            format.json do
              render status: :unprocessable_entity,json: errors_for(@week)
            end
          end
        end
      end

      private

      # Se setea la semana a editar
      def set_week
        @week = Week.find(params[:id])
      end

      def week_params
        params.require(:week).permit!
      end
    end
  end
end
