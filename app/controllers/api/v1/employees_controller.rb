# frozen_string_literal: true


module Api
  module V1
    # Controlador encargado del los empleados de 1 compania
    class EmployeesController < ApplicationController

      # Metodo encargado de Lista los empleado de una Compania
      # GET /api/v1/companies/1/employees.json
      def index
        employees = Employee.where(employee_params)
        render json: employees
      end

      def employee_params
        params.permit(
          :company_id
        )
      end
    end
  end
end
