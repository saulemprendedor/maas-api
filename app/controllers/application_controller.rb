# frozen_string_literal: true

class ApplicationController < ActionController::API
  def errors_for(model)
    { errors: model.errors }
  end
end
