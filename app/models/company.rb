# frozen_string_literal: true

# == Schema Information
#
# Table name: companies
#
#  id            :bigint           not null, primary key
#  name          :string           not null
#  business_time :jsonb
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class Company < ApplicationRecord
  has_many :employees
  has_many :weeks

  def total_hours
    hours = 0
    business_time.each do |day|
      hours += day[1].length
    end
    hours
  end
end
