# frozen_string_literal: true

# == Schema Information
#
# Table name: weeks
#
#  id                    :bigint           not null, primary key
#  company_id            :bigint
#  range                 :string           not null
#  employee_availability :jsonb
#  monitoring_assignment :jsonb
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
class Week < ApplicationRecord
  belongs_to :company

  validates_uniqueness_of :company_id, scope: [:range]

  # Metodo encargado de balancear la carga asignada a los trabajadores
  def balance_assignments
    employees = {}
    company.employees.each do |emp|
      employees[emp.id] = { id: emp.id, assignments_count: 0 }
    end
    self.monitoring_assignment = employee_availability.dup
    matriz = Week.order_matriz(employee_availability)
    matriz.each do |key, row|
      employee_assignment(key, row, employees)
    end
  end

  # Metodo encargado de obtener la cantidad de horas asignada por semana
  def get_count_assignment_emp(employee_id)
    count = 0
    monitoring_assignment.each do |_key, day|
      count += day.select { |_k, v| v == employee_id }.count
    end
    count
  end

  # Metodo encargado de Obtener las semanas por companias
  # Se obtienen las que ya estan registradas + 5 desde la semana actual
  def self.get_ranges(company_id)
    weeks = {}
    Week.where(company_id: company_id).order(:range).each do |week|
      weeks[week.range] = week
    end
    (1..5).each do |pos|
      week_pos = Time.now.strftime('%W').to_i + pos
      range_pos = "#{Date.commercial(Time.now.year, week_pos, 1).strftime('%Y%m%d')}-#{Date.commercial(Time.now.year, week_pos, 7).strftime('%Y%m%d')}"
      weeks[range_pos] = { range: range_pos, company_id: company_id } unless weeks.key?(range_pos)
    end
    weeks.sort_by { |k, _v| k }.to_h
  end


  # Metodo encargado de Ordenar la Matriz de empleados para comenzar asignando
  # El ordenamiento es de menor disponibilidad x dia a mayor
  def self.order_matriz(employee_availability)
    matriz = {}
    employee_availability.each do |day|
      day[1].each do |hour|
        matriz["#{day[0]}-#{hour[0]}"] = hour[1]
      end
    end
    matriz = matriz.sort_by { |_k, v| v.select { |_kk, vv| vv.to_s == 'true' }.count }
  end

  # Metodo encargado de la asignacion de Carga por Bloque de hora
  # Cuando entra en else, se le asigna la hora al Empleado que en ese punto
  # Tiene menos ahora asignadas
  def employee_assignment(key, row, employees)
    row_selected = row.select { |_kk, vv| vv.to_s == 'true' }
    case row_selected.count
    when 0
      monitoring_assignment[key.split('-')[0]][key.split('-')[1]] = nil
    when 1
      monitoring_assignment[key.split('-')[0]][key.split('-')[1]] = row_selected.first[0].to_i
      employees[row_selected.first[0].to_i][:assignments_count] += 1
    else
      emp_selected = employees.select { |_k, v| row_selected.keys.include?(v[:id].to_s) }
      emp_ordered = emp_selected.sort_by { |_k, v| v[:assignments_count] }
      monitoring_assignment[key.split('-')[0]][key.split('-')[1]] = emp_ordered.first[1][:id]
      employees[emp_ordered.first[1][:id].to_i][:assignments_count] += 1
    end
  end

end
