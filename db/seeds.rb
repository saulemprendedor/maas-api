# frozen_string_literal: true

puts 'SEED: COMPANIES'.magenta
puts ('=' * 100).magenta

companies = [
  {
    name: 'turbus.cl',
    created_at: Time.now,
    updated_at: Time.now,
    business_time: {
      0 => [19, 20, 21, 22, 23],
      1 => [19, 20, 21, 22, 23],
      2 => [19, 20, 21, 22, 23],
      3 => [19, 20, 21, 22, 23],
      4 => [19, 20, 21, 22, 23],
      5 => [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
      6 => [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
    }
  },
  {
    name: 'pullman.cl',
    created_at: Time.now,
    updated_at: Time.now,
    business_time: {
      0 => [10, 11, 12, 13, 16, 17, 18, 19, 20],
      1 => [10, 11, 12, 13, 16, 17, 18, 19, 20],
      2 => [10, 11, 12, 13, 16, 17, 18, 19, 20],
      3 => [10, 11, 12, 13, 16, 17, 18, 19, 20],
      4 => [10, 11, 12, 13, 16, 17, 18, 19, 20],
      5 => [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
      6 => [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
    }
  }
]

Company.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('companies')
companies.each do |company|
  Company.create!(company)
end

puts 'SEED: EMPLOYEES'.magenta
puts ('=' * 100).magenta

employees = [
  {
    company_id: 1,
    name: 'Ernesto',
    color: '#ECB477',
    created_at: Time.now,
    updated_at: Time.now
  },
  {
    company_id: 1,
    name: 'Bárbara',
    color: '#B87F9F',
    created_at: Time.now,
    updated_at: Time.now
  },
  {
    company_id: 1,
    name: 'Benjamín',
    color: '#769EE4',
    created_at: Time.now,
    updated_at: Time.now
  },
  {
    company_id: 2,
    name: 'Matias',
    color: '#8D6E63',
    created_at: Time.now,
    updated_at: Time.now
  },
  {
    company_id: 2,
    name: 'Belen',
    color: '#F06292',
    created_at: Time.now,
    updated_at: Time.now
  }
]

Employee.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('employees')
employees.each do |employee|
  Employee.create!(employee)
end

puts 'SEED: WEEKS'.magenta
puts ('=' * 100).magenta

weeks = [
  {
    company_id: 1,
    range: '20200926-20201001',
    employee_availability: {
      0 => {
        19 => { 1 => false, 2 => false, 3 => true },
        20 => { 1 => false, 2 => false, 3 => true },
        21 => { 1 => false, 2 => false, 3 => true },
        22 => { 1 => false, 2 => false, 3 => true },
        23 => { 1 => false, 2 => false, 3 => true }
      },
      1 => {
        19 => { 1 => true, 2 => false, 3 => false },
        20 => { 1 => true, 2 => false, 3 => false },
        21 => { 1 => true, 2 => false, 3 => false },
        22 => { 1 => true, 2 => false, 3 => false },
        23 => { 1 => true, 2 => false, 3 => false }
      },
      2 => {
        19 => { 1 => false, 2 => false, 3 => true },
        20 => { 1 => false, 2 => false, 3 => true },
        21 => { 1 => false, 2 => false, 3 => true },
        22 => { 1 => false, 2 => false, 3 => true },
        23 => { 1 => false, 2 => false, 3 => true }
      },
      3 => {
        19 => { 1 => true, 2 => false, 3 => false },
        20 => { 1 => true, 2 => false, 3 => false },
        21 => { 1 => true, 2 => false, 3 => false },
        22 => { 1 => true, 2 => false, 3 => false },
        23 => { 1 => true, 2 => false, 3 => false }
      },
      4 => {
        19 => { 1 => false, 2 => true, 3 => false },
        20 => { 1 => false, 2 => true, 3 => false },
        21 => { 1 => false, 2 => true, 3 => false },
        22 => { 1 => false, 2 => true, 3 => false },
        23 => { 1 => false, 2 => true, 3 => false }
      },
      5 => {
        10 => { 1 => true, 2 => false, 3 => false },
        11 => { 1 => true, 2 => false, 3 => false },
        12 => { 1 => true, 2 => false, 3 => false },
        13 => { 1 => true, 2 => false, 3 => false },
        14 => { 1 => true, 2 => false, 3 => false },
        15 => {},
        16 => {},
        17 => {},
        18 => { 1 => false, 2 => false, 3 => true },
        19 => { 1 => false, 2 => false, 3 => true },
        20 => { 1 => false, 2 => false, 3 => true },
        21 => { 1 => false, 2 => false, 3 => true },
        22 => { 1 => false, 2 => false, 3 => true },
        23 => { 1 => false, 2 => false, 3 => true }
      },
      6 => {
        10 => { 1 => true, 2 => false, 3 => false },
        11 => { 1 => true, 2 => false, 3 => false },
        12 => { 1 => true, 2 => false, 3 => false },
        13 => { 1 => true, 2 => false, 3 => false },
        14 => { 1 => true, 2 => false, 3 => false },
        15 => {},
        16 => {},
        17 => {},
        18 => { 1 => true, 2 => false, 3 => true },
        19 => { 1 => true, 2 => false, 3 => true },
        20 => { 1 => false, 2 => false, 3 => true },
        21 => { 1 => false, 2 => false, 3 => true },
        22 => { 1 => false, 2 => false, 3 => true },
        23 => { 1 => false, 2 => false, 3 => true }
      }
    },
    monitoring_assignment: nil,
    created_at: Time.now,
    updated_at: Time.now
  }
]

Week.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('weeks')
weeks.each do |week|
  Week.create!(week)
end
