# frozen_string_literal: true

class CreateWeeks < ActiveRecord::Migration[6.0]
  def change
    create_table :weeks do |t|
      t.references :company
      t.string :range, null: false
      t.jsonb :employee_availability
      t.jsonb :monitoring_assignment
      t.timestamps
    end
  end
end
