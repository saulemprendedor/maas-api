# frozen_string_literal: true

class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees do |t|
      t.references :company
      t.string :name, null: false
      t.string :color, null: false
      t.timestamps
    end
  end
end
