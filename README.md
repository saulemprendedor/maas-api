# Maas API

Este Software es para los Trabajadores de la empresa GuardianesInformáticos SpA. Ellos tiene un negocio que ellos llaman MaaS
(Monitoring as a Service) y su personal son expertos ingenieros devops y programadores, y
su tarea es monitorear servicios de otras empresas.

Con Mass podrán:
*Selecionar una compañia.
*Selecionar una semana que se va monitorear.
*Registrar las disponibilidad de horas por cada empleado.
*Balancear la carga semanal.


## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋


```
ruby '2.7.2'
Postgres
```

Run
```
git clone https://saulemprendedor@bitbucket.org/saulemprendedor/maas-api.git
```


### Instalación 🔧

_Instalar dependencias_

Run
```
bundle install
```
crear un fichero .env en la raiz del proyecto, para agregar las variable de entorno
```
DATABASE_NAME=maas_development
DATABASE_HOST=localhost
PORT_DATABASE=5432
DATABASE_USER=user
POSTGRESQL_PASSWORD=pass
```
Run
```
bundle exec rake db:setup
rails s
```

- api-url:   http://localhost:3000

## Ejecutando las pruebas ⚙️

_Las pruebas se hicieron con rspec para validar los metodos de los modelos_

Run
```
bundle exec rspec
```




💎🚀
