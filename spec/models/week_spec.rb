# frozen_string_literal: true

# == Schema Information
#
# Table name: weeks
#
#  id                    :bigint           not null, primary key
#  company_id            :bigint
#  range                 :string           not null
#  employee_availability :jsonb
#  monitoring_assignment :jsonb
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#
require 'rails_helper'

RSpec.describe Week, type: :model do
  it 'get all weeks' do
    weeks = Week.all
    expect(1).to eq weeks.count
  end

  it 'validate balance_assignments' do
    week = Week.find(1)
    week.balance_assignments
    expect(21).to eq week.get_count_assignment_emp(1)
    expect(5).to eq week.get_count_assignment_emp(2)
    expect(21).to eq week.get_count_assignment_emp(3)
  end

  it 'validate get_ranges' do
    weeks = Week.get_ranges(1)
    expect(6).to eq weeks.count
  end
end
