# frozen_string_literal: true

# == Schema Information
#
# Table name: companies
#
#  id            :bigint           not null, primary key
#  name          :string           not null
#  business_time :jsonb
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

RSpec.describe Company, type: :model do
  it 'get all Companies' do
    companies = Company.all
    expect(2).to eq companies.count
  end

  it 'validate total_hours' do
    company = Company.find(1)
    expect(53).to eq company.total_hours
  end
end
